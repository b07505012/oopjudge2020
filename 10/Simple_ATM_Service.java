public class Simple_ATM_Service implements ATM_Service {

    @Override
    //錢夠不夠提
    public boolean checkBalance(Account account, int amount) throws ATM_Exception {
        if(account.getBalance() > amount || account.getBalance() == amount) {
            return true;
        }else {
            throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);
        }
    }

    @Override
    //提1000的倍數
    public boolean isValidAmount(int amount) throws ATM_Exception {
        if(amount%1000 == 0) {
            return true;
        }else {
            throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
        }
    }

    @Override
    //判斷需不需要錯誤訊息
    public void withdraw(Account account, int amount) {
        try {
            checkBalance(account, amount);
            isValidAmount(amount);
            account.setBalance(account.getBalance() - amount);
            System.out.println("updated balance : " + account.getBalance());
        }catch (ATM_Exception a) {
            System.out.println(a.getMessage());
            System.out.println("updated balance : " + account.getBalance());
        }
    }
}
