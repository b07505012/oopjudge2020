//construct an object of IsLeapYear, obj.determine(year) to figure out if that year
//is a leap year
public class IsLeapYear {
	
	public static boolean determine(int year) {
		//this part shows how I judge if the input year is a leap year
		
		if(year%4 == 0 && year%100 != 0) {
			return true;
		}
		else if(year%400 == 0) {
			return true;
		}
		else {
			return false;
		}
	}	
}
