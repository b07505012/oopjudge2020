//construct an object of GreenCrud, obj.calPopulation(initialSize, days) to figure 
//out the size of the green crud population
public class GreenCrud {
	
	public int calPopulation(int initialSize, int days)
	{
		int step = days/5;//the value may change every five days
		int preSize;
		int nowSize = 0;
		int size = initialSize;
		
		for(int i=1; i<=step; i++)
		{
			preSize = nowSize;
			nowSize = size;
			size += preSize;
		}
		
		return size;
		
	}

}
