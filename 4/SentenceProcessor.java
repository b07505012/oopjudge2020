//construct an object of SentenceProcessor, obj.removeDuplicatedWords(sentence)
//to remove the duplicated words in the sentence, obj.replaceWord(target, replacement, 
//sentence) to replace the word into the target words
public class SentenceProcessor {
	
	public String removeDuplicatedWords(String sentence)
	{
		String[] parts = sentence.split(" ");
		int size = parts.length;
		String Answer = parts[0];
		
		for(int i=1; i<size; i++)
		{
			if(! Answer.contains(parts[i]))
			{
				Answer += " " + parts[i];
			}
		}
		return Answer;
	}
	
	public String replaceWord(String target, String replacement, String sentence)
	{
		String[] parts = sentence.split(" ");
		int size = parts.length;
		String Answer = new String();
		for(int i=0; i<(size-1); i++)
		{
			if(! parts[i].equals(target))
			{
				Answer += parts[i] + " ";
			}
			else if(parts[i].equals(target))
			{
				Answer += replacement + " ";
			}
		}
		
		if(! parts[size-1].equals(target))
		{
			Answer += parts[size-1];
		}
		else if(parts[size-1].equals(target))
		{
			Answer += replacement;
		}
		
		return Answer;
	}
}
