public class Pizza {
	
	private String size = "small";
	private int NumberOfCheese = 1;
	private int NumberOfPepperoni = 1;
	private int NumberOfHam = 1;
	private double price = 0;
	
	public Pizza(String size, int NumberOfCheese, int NumberOfPepperoni, int NumberOfHam)
	{
		setSize(size);
		this.NumberOfCheese = NumberOfCheese;
		this.NumberOfPepperoni = NumberOfPepperoni;
		this.NumberOfHam = NumberOfHam;
	}
	
	public Pizza()
	{
	}
	
	public void setSize(String size)
	{
		this.size = size;
	}
	
	public String getSize()
	{
		return size;
	}
	
	public void setNumberOfCheese(int NumberOfCheese)
	{
		this.NumberOfCheese = NumberOfCheese;
	}
	
	public int getNumberOfCheese()
	{
		return NumberOfCheese;
	}
	
	public void setNumberOfPepperoni(int NumberOfPepperoni)
	{
		this.NumberOfPepperoni = NumberOfPepperoni;
	}
	
	public int getNumberOfPepperoni()
	{
		return NumberOfPepperoni;
	}
	
	public void setNumberOfHam(int NumberOfHam)
	{
		this.NumberOfHam = NumberOfHam;
	}
	
	public int getNumberOfHam()
	{
		return NumberOfHam;
	}
	
	public double calcCost()
	{
		price += 2*(NumberOfCheese + NumberOfPepperoni + NumberOfHam);
		if(size.equals("small"))
		{
			price += 10;
		}
		else if(size.equals("medium"))
		{
			price += 12;
		}
		else if(size.equals("large"))
		{
			price += 14;
		}
		double number = price;
		price = 0;
		return number;
	}
	
	public String toString()
	{
		String condition = "size = " + size + ", numOfCheese = " + NumberOfCheese 
				+ ", numOfPepperoni = " + NumberOfPepperoni + ", numOfHam = " + NumberOfHam;
		return condition;
	}
	
	public boolean equals(Pizza P)
	{
		boolean flag = false;
		if(this.size == P.size && this.NumberOfCheese == P.NumberOfCheese)
		{
			if(this.NumberOfHam == P.NumberOfHam && this.NumberOfPepperoni == P.NumberOfPepperoni)
			{
				flag = true;
			}
		}
		return flag;
	}
}
