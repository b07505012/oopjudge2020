public class PizzaOrder {
	
    private int numberPizzas;
    private Pizza pizza1, pizza2, pizza3;
    
    public boolean setNumberPizzas(int numberPizzas){
        if(numberPizzas>=1 && numberPizzas<=3){           
            this.numberPizzas = numberPizzas;
            return true;
        }
        else {
            return false;
        }
    }
    
    public void setPizza1(Pizza pizza1){
        this.pizza1 = pizza1;
    }
    
    public void setPizza2(Pizza pizza2){
        if(numberPizzas>1){
            this.pizza2 = pizza2;
        }
        else {
            System.out.println("You only order one pizza!");
        }
    }
    
    public void setPizza3(Pizza pizza3){
        if(numberPizzas>2){
            this.pizza3 = pizza3;
        }
        else if(numberPizzas==1){
            System.out.println("You only order one pizza!");
        }
        else {
            System.out.println("You only order two pizza!");
        }
    }
    
    public double calcTotal(){
        if(numberPizzas==1){
            return pizza1.calcCost();
        }
        else if(numberPizzas==2){
            return pizza1.calcCost() + pizza2.calcCost();
        }
        else {
            return pizza1.calcCost() + pizza2.calcCost() + pizza3.calcCost();
        }
    }
}
