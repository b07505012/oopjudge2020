public class SimpleArrayList {
	
    private Integer[] array;
    public SimpleArrayList() {
    	
    	//Default constructor that sets the array size to zero.
        array = new Integer[0];
    }

    public SimpleArrayList(int n) {
        
    	//Constructor setting initial array size, and preset all Integer to zero
    	array = new Integer[n];
        for(int i=0;i<n;i++) {
            array[i] = 0;
        }
    }

    public void add(Integer i) {
        
    	//Use a new array to store the original array
    	Integer[] newArray = new Integer[array.length+1];
        System.arraycopy(array, 0, newArray, 0, array.length);
        newArray[newArray.length-1] = i;
        array = newArray;
    }

    public Integer get(int index) {
        
    	//Get the element of this array by the given index, or return null if the position 
    	//is not exist
    	if(index>=0 && index<array.length) {
            return array[index];
        }
        else {
            return null;
        }
    }

    public Integer set(int index, Integer element) {
        
    	//Replace the element at the specified position in this array with the specified 
    	//element, and return the original element
    	if(index>=0 && index<array.length) {
            Integer origin = array[index];
            array[index] = element;
            return origin;

        }else {
        	//If the specified position does not exist, return null.
            return null;
        }
    }

    public boolean remove(int index) {
        
    	if(array[index]==null) {
    		//If there's a null element at the specified position, return false.
            return false;
        }else {
        	//Remove the element at the specified position, 
        	//and shift other elements to the left if the action is successfully.
            Integer[] newArray = new Integer[array.length-1];
            System.arraycopy(array, 0, newArray, 0, index);
            System.arraycopy(array, index+1, newArray, index, newArray.length-index);
            array=newArray;
            return true;
        }
    }

    public void clear() {
    	
    	//Remove all elements from this array.
        array = new Integer[array.length];
    }

    public int size() {
    	
    	//Return the number of elements in this array.
        return array.length;
    }

    public boolean retainAll(SimpleArrayList l) {
    	
        SimpleArrayList newArrayList = new SimpleArrayList();
        int countNull = 0;
        for(int i=0;i<this.size();i++) {
            for(int j=0;j<l.size();j++) {
                if(this.array[i]!=null) {
                    if((this.array[i].equals(l.get(j)))) {
                    	//Use a new array list to store the matched elements.
                        newArrayList.add(l.get(j));
                        break;
                    }
                }else {
                    countNull++;
                    break;
                }
            }
        }
        if(this.size()-countNull==newArrayList.size()) {
        	//A new array to this array.
            this.array = newArrayList.array;
            return false;
        }else {
        	//A new array to this array.
            this.array = newArrayList.array;
            return true;
        }
    }

}
