public class Circle extends Shape {

    public Circle(double length) {
        super(length);
    }

    @Override   //Set length = diameter
    public void setLength(double length) {
        this.length = length;
    }

    @Override   //Calculate size
    public double getArea() {
        return Math.round(Math.PI*Math.pow(length/2,2)*100.0)/100.0;
    }

    @Override    //Calculate perimeter
    public double getPerimeter() {
        return Math.round(Math.PI*length*100.0)/100.0;
    }
}
