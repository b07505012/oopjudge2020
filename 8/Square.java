public class Square extends Shape {

    public Square(double length) {
        super(length);
    }

    @Override   //Set length
    public void setLength(double length) {
        this.length = length;
    }

    @Override   //Calculate size
    public double getArea() {
        return Math.round(Math.pow(length,2)*100.0)/100.0;
    }

    @Override    //Calculate perimeter
    public double getPerimeter() {
        return Math.round(4*length*100.0)/100.0;
    }
}
