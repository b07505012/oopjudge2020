public class Triangle extends Shape {

    public Triangle(double length) {
        super(length);
    }

    @Override   //Set length
    public void setLength(double length) {
        this.length = length;
    }

    @Override   //Calculate size
    public double getArea() {
        return Math.round(Math.sqrt(3)/4*Math.pow(length,2)*100.0)/100.0;
    }

    @Override   //Calculate perimeter
    public double getPerimeter() {
        return Math.round(3*length*100.0)/100.0;
    }
}