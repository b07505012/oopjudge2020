import java.text.DecimalFormat;

public class SimpleCalculator {
    private double result=0.0;
    private int count = 0;
    private String cmd = "";
    private String operator;
    private double value;
    public void calResult(String cmd) throws UnknownCmdException {
    	//Determine the act of the calculator
        if(!endCalc(cmd)) {
            String[] container =  cmd.split(" ");
            //If the input is legal, there must be a operator and a number, in other word, length == 2
            if(container.length != 2) {
                throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
            //If the input is legal, determine the act by +-*/    
            }else if(isNumber(container[1])) {
                value = Double.parseDouble(container[1]);
                operator = container[0];
                if(container[0].equals("+")) {
                	result = result + value;
                    count++;
                }else if(container[0].equals("-")) {
                	result = result - value;
                    count++;
                }else if(container[0].equals("*")) {
                	result = result * value;
                    count++;
                }else if(container[0].equals("/")) {
                	if(value == 0) {
                        throw new UnknownCmdException("Can not divide by 0");
                    }else {
                        result = result / value;
                        count++;
                    }
                }else {
                	throw new UnknownCmdException(container[0] + " is an unknown operator");
                }
            }else {
            	//container[1] is not a legal element (not a number)
                if(container[0].equals("+") || container[0].equals("-") || container[0].equals("*") || container[0].equals("/")) {
                    throw new UnknownCmdException(container[1] + " is an unknown value");
                  //container[0] is not a legal element (not a acceptable operator)
                }else {
                    throw new UnknownCmdException(container[0] + " is an unknown operator and " + container[1] + " is an unknown value");
                }
            }
        }
    }

    public String getMsg() {
        DecimalFormat df = new DecimalFormat("###0.00");
        if(endCalc(cmd)) {
            return "Final result = " + df.format(result);
        }else if(count == 0) {
            return "Calculator is on. Result = 0.00";
        }else if(count == 1) {
            return "Result " + operator + " " + df.format(value) + " = " + df.format(result) + ". New result = " + df.format(result);
        }else {
            return "Result " + operator + " " + df.format(value) + " = " + df.format(result) + ". Updated result = " + df.format(result);
        }
    }

    public boolean endCalc(String cmd) {
        if(cmd.equals("r")) {
            this.cmd = cmd;
            return true;
        }else if(cmd.equals("R")) {
        	this.cmd = cmd;
            return true;
        }else {
            return false;
        }

    }

    public static boolean isNumber(String s){
        try{
            Double.parseDouble(s);
            return true;
        }catch(Exception e){
            return false;
        }
    }
}
